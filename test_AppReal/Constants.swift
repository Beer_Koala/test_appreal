//
//  Constants.swift
//  test_AppReal
//
//  Created by BeerKoala on 7/22/19.
//  Copyright © 2019 a.kryvchykov. All rights reserved.
//

import Foundation

enum Constants: String {
    case person = "Person"
    case number = "number"

    //hello alert
    case welcome = "Welcome"
    case hello = "Hello"
    case welcomeText = "Glad to see you here"

    // identifier
    case toPersonDetail
    case cell = "Cell"

}
