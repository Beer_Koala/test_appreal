//
//  ViewController.swift
//  test_AppReal
//
//  Created by BeerKoala on 7/12/19.
//  Copyright © 2019 a.kryvchykov. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    var list: [Person] = []
    var fetchedResultsController: NSFetchedResultsController<Person>!

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        let managedContext = CoreDataManager.shared.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<Person>(entityName: Person.entity().name ?? Constants.person.rawValue)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: Constants.number.rawValue, ascending: true)]

        do {
            list = try managedContext.fetch(fetchRequest)
        } catch {
            error.showError(title: "Could not fetch.")
        }

        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                              managedObjectContext: managedContext,
                                                              sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController?.delegate = self
        do {
            try fetchedResultsController?.performFetch()
        } catch {
            error.showError(title: "Could not fetch.")
        }

        // for first launch
        if UserDefaults.isFirstLaunch() {
            DispatchQueue.global().async {
                let listNames = ["Adam", "Henry", "Jeremy", "Kyle", "Mark", "Douglas", "Marion",
                                 "Jade", "Madison", "Robert", "Peyton", "Rodney", "Lucas",
                                 "Sam", "Eugene", "Laurie", "Jason", "Edward", "Toby", "Johnny"]

                var iterator = 1
                for name in listNames {
                    _ = Person(name: name, number: iterator, with: managedContext)
                    iterator += 1
                }
                CoreDataManager.shared.saveContext()

                DispatchQueue.main.async {
                    UIAlertController.simple(title: Constants.welcome.rawValue,
                                             message: Constants.welcomeText.rawValue,
                                             okText: Constants.hello.rawValue,
                                             to: self)
                }
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == Constants.toPersonDetail.rawValue,
            let destinationVC = segue.destination as? PersonViewController,
            let cell = sender as? TableViewCell,
            let person = cell.person
            else { return }

        destinationVC.person = person
        destinationVC.title = person.name
    }

}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController?.fetchedObjects?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: Constants.cell.rawValue, for: indexPath) as? TableViewCell ?? TableViewCell()

        cell.person = fetchedResultsController?.fetchedObjects?[indexPath.row]

        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ViewController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {

        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .automatic)
            }
        case .update:
            if let indexPath = indexPath {
                let person = fetchedResultsController.object(at: indexPath)
                guard let cell = tableView.cellForRow(at: indexPath) as? TableViewCell else { break }
                cell.person = person
            }
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        @unknown default:
            do {} // do nothing
        }
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
       tableView.endUpdates()
    }
}
