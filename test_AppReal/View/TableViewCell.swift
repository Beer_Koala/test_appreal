//
//  TableViewCell.swift
//  test_AppReal
//
//  Created by BeerKoala on 7/16/19.
//  Copyright © 2019 a.kryvchykov. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    var person: Person? {
        didSet {
            //prepare
            self.nameLabel.text = ""
            self.avatarImageView.image = nil
            activityIndicator.startAnimating()

            //set values
            self.nameLabel?.text = person?.name
            person?.image { [weak self] (person) in
                DispatchQueue.main.async {
                    if self?.person == person { // check person for quick scroll
                        self?.activityIndicator.stopAnimating()
                        if let avatarImageData = person.avatar?.image,
                            let image = UIImage(data: avatarImageData) {
                            self?.avatarImageView?.image = image
                        } else {
                            self?.avatarImageView?.image = #imageLiteral(resourceName: "sadSmartphone")
                        }
                    }
                }
            }
        }
    }
}
