//
//  PersonViewController.swift
//  test_AppReal
//
//  Created by BeerKoala on 7/20/19.
//  Copyright © 2019 a.kryvchykov. All rights reserved.
//

import UIKit

class PersonViewController: UIViewController {

    var person: Person?
    @IBOutlet weak var nameTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        if let person = person {
            nameTextField.text = person.name
        }
    }

    override func didMove(toParent parent: UIViewController?) {
        if nameTextField.text != person?.name {
            person?.name = nameTextField.text
            do {
                try person?.managedObjectContext?.save()
            } catch {
                error.showError(title: "Could not fetch.")
            }
        }
    }
}
