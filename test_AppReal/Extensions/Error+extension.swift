//
//  Error+extension.swift
//  test_AppReal
//
//  Created by BeerKoala on 7/22/19.
//  Copyright © 2019 a.kryvchykov. All rights reserved.
//

import Foundation
import UIKit

extension Error {

    func showError(title: String = "Unresolved error.") {
        if let error = self as NSError? {
            UIAlertController.simple(title: title,
                                     message: "\(error), \(error.userInfo)",
                okText: "OK",
                to: UIApplication.shared.windows.first?.visibleViewController)
        }
    }
}
