//
//  UIAlertController+extension.swift
//  test_AppReal
//
//  Created by BeerKoala on 7/22/19.
//  Copyright © 2019 a.kryvchykov. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
    class func simple(title: String, message: String, okText: String, to viewController: UIViewController?) {
        if let viewController = viewController {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: okText, style: .default, handler: nil))

            viewController.present(alert, animated: true)
        }
    }
}
