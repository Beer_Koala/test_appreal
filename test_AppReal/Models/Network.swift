//
//  Network.swift
//  test_AppReal
//
//  Created by BeerKoala on 7/15/19.
//  Copyright © 2019 a.kryvchykov. All rights reserved.
//

import UIKit

class Network: NSObject {

    // singletone
    static let shared = Network()
    private override init() {
        self.configuration = URLSessionConfiguration.background(withIdentifier: sessionConfigurationIdentifier)
        self.configuration.httpMaximumConnectionsPerHost = 1

        super.init()

        self.session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
    }

    var activeDownloads: [URLSessionDownloadTask: (UIImage?) -> Void] = [:]

    private let sessionConfigurationIdentifier = Bundle.main.bundleIdentifier ?? ""
    private let configuration: URLSessionConfiguration
    private var session: URLSession!

    func getImage(from url: URL, completion: @escaping (UIImage?) -> Void) {
        let downloadTask = session.downloadTask(with: url)
        activeDownloads[downloadTask] = completion
        downloadTask.resume()
    }

}

extension Network: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {

        if let data = try? Data(contentsOf: location),
            let image = UIImage(data: data) {
            // if not image so this is error and no need clear activeDownloads, it cleared below in didCompleteWithError
            let completion = activeDownloads[downloadTask]
            activeDownloads.removeValue(forKey: downloadTask)

            completion?(image)
        }
    }

    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        guard let downloadTask = task as? URLSessionDownloadTask else { return }
        let completion = activeDownloads[downloadTask]
        activeDownloads.removeValue(forKey: downloadTask)

        completion?(nil)
    }

    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        DispatchQueue.main.async {
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
                let completionHandler = appDelegate.backgroundSessionCompletionHandler {
                appDelegate.backgroundSessionCompletionHandler = nil
                completionHandler()
            }
        }
    }

}
