//
//  Person.swift
//  test_AppReal
//
//  Created by BeerKoala on 7/15/19.
//  Copyright © 2019 a.kryvchykov. All rights reserved.
//

import UIKit
import CoreData

extension Person {
    var address: URL {
        var numberString = number.description
        if numberString.count == 1 {
            numberString = "0" + numberString
        }
        return URL(string: "http://download.glide.me/pre-login-avatars/avatars_cartoon_animals_\(numberString).png")!
    }

    convenience init(name: String, number: Int, with context: NSManagedObjectContext) {

        self.init(context: context)

        self.name = name
        self.number = Int16(number)
    }

    func image(completion: @escaping (Person) -> Void) {

        if self.avatar != nil {
            completion(self)
        } else {
            Network.shared.getImage(from: self.address) { loadedImage in

                if let image = loadedImage {
                    let context = CoreDataManager.shared.persistentContainer.viewContext
                    let avatar = Avatar(context: context)
                    avatar.image = image.jpegData(compressionQuality: 1)
                    avatar.person = self

                    self.avatar = avatar

                    CoreDataManager.shared.saveContext()
                }
                completion(self)
            }
        }
    }

}
