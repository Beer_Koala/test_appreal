//
//  CoreDataStack.swift
//  test_AppReal
//
//  Created by BeerKoala on 7/20/19.
//  Copyright © 2019 a.kryvchykov. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager {

    static let shared = CoreDataManager()

    private init() {
    }

    // MARK: - Core Data stack functional
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "test_AppReal")
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error {
                error.showError()
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                error.showError()
            }
        }
    }
}
